# Índice
[Índice](./SUMMARY.md)

# Recetas

1. [Básicos](./basicos/basicos.md)
    - [Arroz al vapor](./basicos/arroz_al_vapor.md)
    - [Cous-Cous](./basicos/couscous.md)
    - [Garbanzos cocidos](./basicos/garbanzos_cocidos.md)
    - [Guisantes con jamón](./basicos/guisantes_con_jamon.md)

2. [Platos Principales](./platos_principales/platos_principales.md)
    - [Berenjenas a la naranja](./platos_principales/berenjenas_naranja.md)
    - [Boquerones fritos](./platos_principales/boquerones_fritos.md)
    - [Curry_23_08_20](./platos_principales/curry_23_08_20.md)
    - [Espinacas con pasas y anacardos](./platos_principales/espinacas_pasas_anacardos.md)
    - [Gazpagendro](./platos_principales/gazpagendro.md)
    - [Pisto](./platos_principales/pisto.md)

3. [Conservas](./conservas/conservas.md)
    - [Zanahorias al vinagre](./conservas/zanahorias_al_vinagre.md)

---
# Mi Cocina
- [Instrumentación](./mi_cocina/instrumentacion.md)
- [Contribuir](./mi_cocina/contribuir.md)
- [Referencias útiles](./mi_cocina/referencias_utiles.md)
