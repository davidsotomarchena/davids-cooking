# Contribuir

Este recetario se elabora con [mdbook](https://rust-lang.github.io/mdBook/).
También se utiliza el preprocesador que se puede encontrar en
<https://gitlab.com/davidsotomarchena/mdbook-davids_cooking>

Simplemente, haz un pull request con tu receta o crea un issue sobre la receta
que te esté dando problemas e intentaré probar la receta o responder lo antes
posible. El repositorio es <https://gitlab.com/davidsotomarchena/davids-cooking>

