# Instrumentación

- Cuchillos
- Tablas de cortar
- Pasa purés
- [Moulinex ClickCheff](https://www.moulinex.es/p/clickchef/9100042747)
- Olla ultra-rápida Magefesa Favorite-6
- Juego de ollas de ikea 365

# Conversión Moulinex ↔ Thermomix

El espectro de velocidades de la Moulinex es de 0 a 12 y la Thermomix de 0 a 10,
con un motor de 500 W.
Para convertir velocidades entre uno y otro basta con multiplicar o dividir por
1.2

| Velocidad inicial | Velocidad destino | Operación                    |
|-------------------|-------------------|------------------------------|
| Thermomix         | Moulinex          | \\(v_m = v_t \cdot 1.2\\)    |
| Moulinex          | Thermomix         | \\(v_t = \frac{v_m}{1.2} \\) |

| Moulinex | Moulinex | Thermomix |
|----------|----------|-----------|
| 0    | 0  |  0   |
| 1    | 1  |  0.8 |
| 1.2  | 1  |  1   |
| 2    | 2  |  1.7 |
| 2.4  | 2  |  2   |
| 3    | 3  |  2.5 |
| 3.6  | 4  |  3   |
| 4    | 4  |  3.3 |
| 4.8  | 5  |  4   |
| 5    | 5  |  4.2 |
| 6    | 6  |  5   |
| 7    | 7  |  5.8 |
| 7.2  | 7  |  6   |
| 8    | 8  |  6.7 |
| 8.4  | 8  |  7   |
| 9    | 9  |  7.5 |
| 9.6  | 10 |  8   |
| 10   | 10 |  8.3 |
| 10.8 | 11 |  9   |
| 11   | 11 |  9.2 |
| 12   | 12 |  10  |


La temperatura Varoma de las antiguas Thermomix iban de 100 ºC a 120 ºC
Por lo general se puede sustituir por 110 ºC

La Moulinex no tiene giro a la izquierda sino que se le añade un plástico que
cubre las cuchillas.
