# Zanahorias al vinagre

## Instrumentos

- Cazuela
- Bote de cristal (muy limpio)

## Ingredientes

- 1 parte agua
- 1/3 parte de vinagre
- Especias (comino, ajo, orégano, tomillo)
- Zanahorias
- Sal, 1 cucharada sopera por cada litro de agua.

## Receta

1. Cortar la zanahoria en bastoncillos que quepan en el bote.
2. Meter los bastoncillos en el bote.
3. Llenar el bote de agua.
4. Verter el agua en la cazuela.
5. Llenar un tercio del bote con vinagre.
6. Verter el vinagre en la cazuela.
7. Añadir la sal y las especias y poner a hervir.
8. Cuando rompa a hervir, meter las zanahorias en la cazuela.
9. Esperar a que vuelva a hervir.
10. Esperar 10 minutos con el agua hirviendo.
11. Poner el bote en el fregadero.
12. Pescar las zanahorias con una espumadera y meterlas en el bote.
13. Utilizar una cuchara para meter todas las especias posibles en el bote.
14. Meter líquido hasta cubrir.
15. Dejar enfriar al natural un par de horas.
16. Tapar y conservar en el frigo.

## Producción

- Un bote de conserva

## Referencias & Notas

Si se usan ajos, machacarlos para que suelten sus aceites. Basta con utilizar
la parte plana de un cuchillo para aplastarlos con la mano.

## Etiquetas

- Vegano
- Sin Lactosa
