# Arroz al vapor

## Instrumentos

- Robot de cocina
- Cestillo interno para cocinar al vapor

## Ingredientes

- 50 g de aceite de oliva virgen extra
- 3 dientes de ajo
- 1 litro de agua
- 1 pastilla de caldo
- 1 cuchara pequeña de sal
- 350 g de arroz

## Receta


1. Poner en el vaso el aceite y los ajos. <step 4 4 min 110/>
2. Añadir el agua y la sal y las pastillas. <step 7 10 sec 0/>
3. Poner el cestillo dentro del vaso. <step 4 7 min 110/>
4. Echa el arroz a través del bocal y mover de vez en cuando. <step 4 13 min 110/>

## Producción

- Unos 700 g de arroz
- ¿Salsa?

## Referencias & Notas

<https://www.velocidadcuchara.com/arroz-blanco-de-guarnicion/>


## Etiquetas

- Vegano
- Sin Lactosa
