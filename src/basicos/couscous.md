# Cous-Cous

## Instrumentos

- Cazo

## Ingredientes
- 60 g/pp persona cous-cous
- 90 g/pp agua
- aceite de oliva
- sal

## Receta
1. Pon el agua con la sal a hervir.
2. Cuando esté en plena ebullición quita la cazuela del fuego.
3. Echar el cous-cous e inmediatamente empezar a remover para que todo el
    cous-cous quede cubierto de agua.
4. Echar un chorreoncillo de aceite y seguir removiendo hasta que no quede
    líquido.

## Producción

120g/pp de couscous

## Referencias & Notas

Si se va a hacer mucho cous-cous es preferible una cacerola, el cous-cous
debería alcanzar poca profundidad. De esa forma el cocido es más uniforme y no
se queda nada con una textura un poco más desagradable

## Etiquetas

- Vegano
- Sin Lactosa
