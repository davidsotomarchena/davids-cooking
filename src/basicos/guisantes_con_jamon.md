# Guisantes con jamón

## Instrumentos
- Cazo
- Sartén
- Tabla de cortar
- Cuchillo

## Ingredientes
- 200 g guisantes congelados
- sal
- 1/2 cebolla
- 1 diente de ajo
- taquitos de jamón
- 1 huevo

## Receta

1. Cortar la cebolla en trocitos chicos y los ajos.
2. Poner la cebolla a freír.
3. En paralelo, poner a hervir el agua con sal al gusto.
4. En cuanto empiece a transparentar la cebolla, añadir los ajos.
5. Dejar dorando a fuego bajo.
6. Cuando el agua llegue a ebullición, poner los guisantes.
7. Cuando el agua vuelva a hervir, poner el huevo y esperar cinco minutos.
8. Escurrir los guisantes y el huevo.
9. Poner los guisantes con la cebolla y el ajo, añadir el jamón y dar una
    vuelta por la sartén, refriendo un poco los guisantes.
10. Pelar el huevo y añadir.

## Producción

300 g de guisantes con jamón.

## Referencias & Notas

## Etiquetas

- Sin lactosa, dependiendo del jamón.
