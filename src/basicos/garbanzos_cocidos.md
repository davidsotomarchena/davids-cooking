# Garbanzos cocidos

## Instrumentos
- Olla
- Colador

## Ingredientes

- Garbanzos
- hojas de laurel al gusto (yo uso una)
- Sal

## Receta

1. Poner los garbanzos secos a rehidratar con más del triple de agua en volumen
    la noche anterior.
2. Escurrir los garbanzos.
3. Poner agua a hervir con el laurel.
4. Cuando haya roto a hervir poner los garbanzos y quitar a la espuma que salga
    durante los primeros minutos, para que cuando se cierre la tapa de la olla
    no salga espuma.
5. Bajar a fuego bajo, como al dos o al tres y dejar durante una 1h 30 min,
    aproximadamente, dependiendo de los garbanzos y la textura deseada.
6. Escurrir los garbanzos.

## Producción

Aproximadamente el triple de garbanzos en volumen.

## Referencias & Notas

<https://www.pequerecetas.com/receta/cocer-garbanzos/>

## Etiquetas

- Vegano
- Sin Lactosa
