# Pisto

## Instrumentos

- Robot de cocina
- Tabla de cortar
- Cuchillo

## Ingredientes

- 1 cebolla
- 1 pimiento rojo
- 1 o 2 dientes de ajo
- 50 ml aceite de oliva
- 1 calabacín verde
- 1 berenjena
- 800 ml de tomate triturado (1 lata)
- especias al gusto (pimienta, cayenas, tabasco)
- sal
- pastilla de caldo

## Receta

1. Preparar el calabacín y la berenjena, pelándolos y cortándolos en cubitos de
menos de 2 cm de ancho. Reservar en el frigo para más adelante.
1. Pelar las cebollas y los ajos y sacar las semillas y lavar el pimiento rojo.
Cortarlo todo en trozos gordos y meter en el robot de cocina.<step 7 20 sec 0/>
1. Añadir el aceite <step 5 7 min 100/>
1. Añadir el tomate triturado, las especias, la pastilla de caldo, la sal, la
berenjena y el calabacín. Mezclarlo todo muy bien con la espátula. <step 1 30 min 100/>

## Producción

\[1.5,2\] L de pisto aprox

## Referencias & Notas


## Etiquetas

- Vegano
- Sin Lactosa
