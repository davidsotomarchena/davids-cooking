# Boquerones fritos

## Instrumentos

- 2 Tupper de poco fondo
- Sartén pequeña

## Ingredientes

- boquerones
- Hielo
- harina (harina o garbanzo)
- sal
- bastante aceite para freír

## Receta

1. Poner en un tupper agua con hielo.
2. Poner medio dedo de aceite en la sartén y poner el fuego al máximo.
3. Poner los boquerones en el agua con hielo un par de minutos.
4. Poner poca harina en el segundo tupper, como para apenas cubrir el fondo y
    sal al gusto.
5. Poner los boquerones en el tupper de la harina y agitar.
7. Freír los boquerones por ambos lados entre uno y dos de minutos cada lado.
8. Poner en papel de cocina los boquerones según vayan saliendo para secarlos.

## Producción

Lo que traigas de boquerones

## Referencias & Notas

<https://www.directoalpaladar.com/recetas-de-pescados-y-mariscos/como-hacer-mejores-boquerones-fritos-casa>

## Etiquetas

- Sin Lactosa
