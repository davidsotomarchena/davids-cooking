# Berenjenas a la naranja

## Instrumentos

- Sartén
- Exprimidor
- Tablas de cortar
- Cuchillos

## Ingredientes

- 1 berenjena
- 3 dientes de ajo
- 3 naranjas de zumo

## Receta

1. Preparar las berenjenas en rodajas finas (4mm como mucho).
2. Cortar los ajos en trocitos chicos.
3. Empezar a sofreir los ajos y al poco añadir la berenjenas.
4. Ir exprimiendo las naranjas mientras se hacen las berenjenas.
5. Cuando las berenjenas estén listas para comer, añadir el zumo. Debería cubrir
a las berenjenas.
6. Esperar a que espese el zumo y empieze a caramelizarse.
7. Cuando empieze a quedar un poco sólido en el fondo de la sarten sacar
inmediatamente.

## Producción

- 2 guarniciones de berenjenas

## Referencias & Notas

Se pueden añadir taquitos de jamón entre los pasos 6 y 7, pero deja de ser
vegano.

## Etiquetas

- Vegano
- Sin Lactosa
