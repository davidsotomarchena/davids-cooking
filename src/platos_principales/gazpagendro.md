# Gazpagendro

## Instrumentos

- Cuchillos
- Tabla de cortar
- Robot de cocina o batidora
- Pasa purés (opcional)

## Ingredientes

- 1 pepino
- 1 pimiento de freír chico
- \[1,3\] dientes ajo
- \[1, 2\] kg de tomates
- sal
- \[50,100\] mL aceite de oliva
- 100 g pan

## Receta

1. Pasar por la máquina el pepino, el pimiento y los ajos <step 8 1 min 0/>
2. Añadir el tomate cortado en cuartos, la sal y añadir el aceite poco a poco
hacia el final de este paso <step 8 5 min 0/>
3. Añadir el pan y corregir el punto de Sal <step 8 1 min 0/>
4. Opcional, pasar por el pasa purés.

## Producción

1.2 L de gazpagendro

## Referencias & Notas

Usar tomates buenos. Lo bueno que está depende directamente de los tomates.

## Etiquetas

- Vegano
- Sin Lactosa
