# Espinacas con pasas y anacardos

## Instrumentos
- Sartén grande
- Tabla de cortar
- Cuchillo

## Ingredientes

- 1/2 cebolla
- 3 dientes de ajo
- 300 g de espinacas en hoja.
- Pasas
- Anacardos
- Miel
- Vinagre

## Receta

1. Cortar la cebolla en trozos chicos.
2. Cortar el ajo.
3. Freír la cebolla hasta que esté transparente.
4. Añadir los ajos.
5. Esperar a que empiecen a dorarse.
6. Añadir las espinacas.
    > Puede que sea necesario añadirlas en varias veces y esperar a que bajen
7. Cuando se hayan sofrito un poco las espinacas, añadir las pasas y los
    anacardos. Se pueden machacar un poco de ante mano e incluso darles un
    picado rápido, dependiendo de la textura que se quiera.
8. Tras freír un poco los anacardos y las pasas con las espinacas, echar un
    chorreón de miel y un poco de vinagre y esperar a que se evapore el vinagre.

## Producción

Unos 300 g de espinacas.

## Referencias & Notas

Este plato se puede engordar con arroz o cous-cous. Personalmente, prefiero con
arroz al vapor.

## Etiquetas

- Vegetariano
- Vegano (Si se omite la miel)
- Sin lactosa

