# Curry_23_08_20


## Instrumentos

- Tabla de cortar
- Cuchillos
- Sartén grande
- Espátula

## Ingredientes

- 300g panceta
- 1 cebolla
- 5 Pimientos picantes
- 3 dientes de ajo
- Jengibre al gusto
- 1 cucharada sopera de curry
- 1 Calabacín verde
- 2 tomates
- Una pastilla de caldo vegetal
- 500mL de agua
- 150mL Leche de Coco

## Receta

1. Cortar la panceta en trozos que se puedan comer con una cuchara
2. Cortar la cebolla y los pimientos en trocitos chicos
3. Freír un poco la panceta y reservar.
4. Sofreir la cebolla con los pimientos hasta que la cebolla comienze a
    pocharse.
5. Mientras tanto cortar el jengibre en bastoncillos y los tomates y el
    calabacín en daditos.
5. Añadir el ajo, el curry y el jengibre al sofrito. Y pasar por unos segundos
6. Añadir las verduras y la carne y freir ligeramente.
7. Añadir el agua y la pastilla y llevar a hervir, en ese momento bajar el
    fuego y dejar cocer a fuego lento durante unos 20 minutos.
8. Sacar del fuego y añadir la leche de coco y mezclar con la espátula.


## Producción

2L de curry, que se pueden engordar con arroz o Cous-Cous.

## Referencias & Notas

<https://www.tasteofhome.com/article/how-to-make-curry/>

Curry es un montón de cosas, este es el curry que hice el 20 de agosto de 2023,
modifica como quieras la receta.

El jengibre perderá mucha intensidad, no es una locura dejarlo en bastoncitos
para que haya trocitos con extra de jengibre. No satura el sabor al estar
hervido.

Es mejor si los tomates tienen poco líquido.

Servir con arroz o Cous-Cous.

La leche de coco sobrante se puede poner con una ensalada de fruta con
cereales.

## Etiquetas

- Sin Lactosa
